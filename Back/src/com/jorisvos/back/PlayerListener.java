package com.jorisvos.back;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class PlayerListener implements Listener {
	@EventHandler
	public void OnPlayerTeleports(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		MainClass.playersMap.put(player, player.getLocation());
	}
	
	@EventHandler
	public void OnPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		MainClass.playersMap.put(player, player.getLocation());
	}
}
