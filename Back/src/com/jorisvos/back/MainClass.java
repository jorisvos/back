package com.jorisvos.back;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class MainClass extends JavaPlugin {
	public static Map<Player, Location> playersMap = new HashMap<Player, Location>();
	ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	public void onEnable()
	{
		console.sendMessage(ChatColor.BLUE + "[Back] " + ChatColor.GREEN + "Back version V"+getDescription().getVersion()+" is enabled!");
	    getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}
		
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("back"))
		{
			if (!(sender instanceof Player))
				sender.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "[Back] " + ChatColor.RED + "You have to be a player to execute this command!");
			else
			{
				Player player = (Player)sender;
				
				if (!(MainClass.playersMap.containsKey(player)))
					player.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "[Back] " + ChatColor.BLUE + "You have no last location!");
				else
				{
					player.teleport(playersMap.get(player));
					player.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "[Back] " + ChatColor.GREEN + "You have been teleported!");
				}
			}
			
			return true;
		}
		return false;
	}
	
	public void onDisable() { console.sendMessage(ChatColor.BLUE + "[Back] " + ChatColor.RED + "Back version V"+getDescription().getVersion()+" is disabled!"); }
}
